# Smart Trash Collection
Plataforma distribuida para la recolección en ruta de desperdicios de ciudades superpobladas

### El Problema
***
El principal problema que se puede destacar es la escasa y poco eficiente recolección de basura debido a la excesiva cantidad de desperdicios provocados por la inconsciencia de las personas, la cual ha provocado graves problemas de contaminación en las grandes ciudades, la manera en que se desea aplicar una contramedida es implementando una arquitectura inteligente y escalable.
Con una recolección de basura más eficiente y optimizada, la reducción de tiempo, costo de recolección traen consigo el promover en gran medida el desarrollo de ciudades inteligentes y sus métodos o estándares de convivencia.
Esta implementación llamada SYSTEM CALL: STC (SMART TRASH COLLECTION) es una arquitectura avanzada, destinada a resolver los problemas ambientales al reducir los desechos detectando contenedores sobrecargados, mediante una arquitectura distribuida que cuenta con una cola de mensaje RabbitMQ.
Por lo tanto, al optar por la aplicación de esta arquitectura, se debe considerar los avances en el desarrollo de este proyecto, esta arquitectura escalable es útil para ciudades que quieren reducir el impacto ambiental y mejorar el estilo de vida con una herramienta eficaz.
