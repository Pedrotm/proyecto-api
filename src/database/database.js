const mongoose = require('mongoose');
const MONGO_URI = "mongodb://Admin:12345@cluster-shard-00-00.d1cgy.mongodb.net:27017,cluster-shard-00-01.d1cgy.mongodb.net:27017,cluster-shard-00-02.d1cgy.mongodb.net:27017/Proyecto?ssl=true&replicaSet=atlas-zxmgv4-shard-0&authSource=admin&retryWrites=true&w=majority"
const connectDB = async () => {
    try{
        // mongodb connection string
        const con = await mongoose.connect(MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        })

        console.log(`MongoDB connected : ${con.connection.host}`);
    }catch(err){
        process.exit(1);
    }
}

module.exports = connectDB 