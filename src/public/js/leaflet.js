if (window.location.pathname == "/rutas") {
    const map = L.map('map-template').setView([-2.176297, -79.891591], 10);
    const tileurl = 'https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png';
    const attribution = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    var popup = L.popup();
    var legend = L.control({ position: 'bottomright' });
    //Map - LeafletJS
    L.tileLayer(tileurl, { attribution: attribution }).addTo(map);

    //Clic en el Mapa
    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("Click en: " + e.latlng.toString())
            .openOn(map);
    }

    map.on('click', onMapClick);

    getData();

    //Rounting 
    async function getData() {
        const response = await fetch('/api/sensors');
        const data = await response.json();
        const waypoints = [];
        j = 0;
        for (let i = 0; i < data.length; i++) {
            //Custom Icon - Level (Inactivo - LLeno - Medio - Vacio)
            if (data[i].Estado != 'Activo' && data[i].Nivel == 0) {
                customIcon = new L.Icon({ iconUrl: '/Imagenes/contenedor-inactivo.png', iconSize: [50, 50], iconAnchor: [25, 50] })
                
            } if (data[i].Estado == 'Activo' && data[i].Nivel >= 75) {
                customIcon = new L.Icon({ iconUrl: '/Imagenes/contenedor-lleno.png', iconSize: [50, 50], iconAnchor: [25, 50] })
                
            } if (data[i].Estado == 'Activo' && data[i].Nivel > 35 && data[i].Nivel < 75) {
                customIcon = new L.Icon({ iconUrl: '/Imagenes/contenedor-medio.png', iconSize: [50, 50], iconAnchor: [25, 50] })
                
            } if (data[i].Estado == 'Activo' && data[i].Nivel <= 35) {
                customIcon = new L.Icon({ iconUrl: '/Imagenes/contenedor-vacio.png', iconSize: [50, 50], iconAnchor: [25, 50] })
                
            }
            //Marker
            L.marker([data[i].Altitud, data[i].Longitud], { icon: customIcon })
                .bindPopup('<b>Contenedor: ' + (i + 1) + '</b><br>Estado: ' + data[i].Estado + '</b><br>Nivel: ' + data[i].Nivel)
                .addTo(map);
            //Routing - Only Active and Level > 75 
            if (data[i].Estado == 'Activo' && data[i].Nivel >= 75) {
                waypoints[j] = L.latLng(data[i].Altitud, data[i].Longitud);
                j++;
            }
        }
        //Routing Options
        const Ruta = new L.Routing.control({
            waypoints: waypoints,
            language: 'es',
            draggableWaypoints: false,
            addWaypoints: false,
            showAlternatives: true,
            collapsible:true,
            createContainer:'#controls',
            createMarker: function () { return null; }
        });
        Ruta.addTo(map);
        //Legend - Colors
        legend.onAdd = function (map) {

            var div = L.DomUtil.create('div', 'info legend'),
                colors = ['#BD3434', '#D4C22A', '#34BD9C', '#ffffff']
                labels = ['Lleno', 'Medio', 'Bajo', 'Inactivo']
                levels = ['75% - 100%', '35% - 75%', '0% - 35%', '']

            // loop through our density intervals and generate a label with a colored square for each interval
            div.innerHTML += '<p style="text-align: center; font-weight: bold;">Niveles de Llenado<p>'
            for (let i = 0; i < colors.length; i++) {
                div.innerHTML +=
                    '<i style="background: ' + (colors[i]) + '"></i> &nbsp' +
                    labels[i] +' &nbsp'+ levels[i]+ '<br>';
            }

            return div;
        };

        legend.addTo(map);

        //Elimina el Routing dentro del Mapa
        /*var routingControlContainer = Ruta.getContainer();
        var controlContainerParent = routingControlContainer.parentNode;
        controlContainerParent.removeChild(routingControlContainer);
        var itineraryDiv = document.getElementById('controls');
        itineraryDiv.appendChild(routingControlContainer);*/
    }
}
