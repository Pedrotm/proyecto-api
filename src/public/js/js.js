$("#add_sensors").submit(function(event){
    alert("Sensor creador Correctamente!");
})

$("#update_sensors").submit(function(event){
    event.preventDefault();

    var unindexed_array = $(this).serializeArray();
    var data = {}

    $.map(unindexed_array, function(n, i){
        data[n['name']] = n['value']
    })
    console.log(data);

    var request = {
        "url" : `http://localhost:8080/api/sensors/${data.id}`,
        "method" : "PUT",
        "data" : data
    }

    $.ajax(request).done(function(response){
        alert("Sensor Actualizado Correctamente!");
    })

})

if(window.location.pathname == "/datos"){
    $ondelete = $(".table tbody td a.delete");
    $ondelete.click(function(){
        var id = $(this).attr("data-id")

        var request = {
            "url" : `http://localhost:8080/api/sensors/${id}`,
            "method" : "DELETE"
        }

        if(confirm("¿Seguro que quieres eliminar el Sensor?")){
            $.ajax(request).done(function(response){
                alert("Data Deleted Successfully!");
                location.reload();
            })
        }

    })
}

if(window.location.pathname == "/add-sensors" || window.location.pathname == "/update-sensors"){
window.onload = function(){
    var fecha = new Date(); //Fecha actual
    var mes = fecha.getMonth()+1; //obteniendo mes
    var dia = fecha.getDate(); //obteniendo dia
    var ano = fecha.getFullYear(); //obteniendo año
    var hora = fecha.getHours();
    var min = fecha.getMinutes();
    var seg = fecha.getSeconds();
    if(dia<10)
      dia='0'+dia; //agrega cero si el menor de 10
    if(mes<10)
      mes='0'+mes //agrega cero si el menor de 10
    if(hora<10)
      hora='0'+hora //agrega cero si el menor de 10
    if(min<10)
      min='0'+min //agrega cero si el menor de 10
    if(seg<10)
      seg='0'+seg //agrega cero si el menor de 10
    document.getElementById('date').value=dia+"-"+mes+"-"+ano;
    document.getElementById('time').value=hora+":"+min+":"+seg;
  }
} 