const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    
    Distancia: {
        type: Number,
        required: true
    },
    Altitud: {
        type: Number,
        required: true
    },
    Longitud : {
        type: Number,
        required: true
    },
    Nivel : {
        type: Number,
        required: true
    },
    Estado : {
        type: String,
        required: true
    },
    Hora : {
        type: String,
        required: true
    },
    Fecha : {
        type: String,
        required: true
    } 
})

var Sensordb = mongoose.model('Proyecto', schema);

module.exports = Sensordb;