const axios = require('axios');

exports.datos = (req, res) => {
    // Make a get request to /api/sensors
    axios.get('http://localhost:8080/api/sensors')
        .then(function(response){
            
            res.render('../view/Datos.html', { sensors : response.data, title: 'Datos'});
        })
        .catch(err =>{
            res.send(err);
        })

    
}

exports.rutas = (req, res) => {
    // Make a get request to /api/sensors
    axios.get('http://localhost:8080/api/sensors')
        .then(function(response){
            res.render('../view/Rutas.html', { sensors : response.data.params, title: 'Rutas'});
        })
        .catch(err =>{
            res.send(err);
        })

    
}

exports.add_sensor = (req, res) =>{
    res.render('../view/add-sensors.html',{title: 'Agregar sensor'});
}

exports.update_sensor = (req, res) =>{
    axios.get('http://localhost:8080/api/sensors', { params : { id : req.query.id }})
        .then(function(sensordata){
            res.render("../view/update-sensors.html", { sensors : sensordata.data, title:'Actualizar Sensor'})
        })
        .catch(err =>{
            res.send(err);
        })
}