const express = require('express');
const router = express.Router();
const services = require('../services/render');
const controller = require('../controller/controller');

//Routes - Home
router.get('/', (req, res)=>{
    res.render('Home.html', {title: 'Home'});
});

//Routes - Datos
router.get('/datos', services.datos);

//Routes - Rutas
router.get('/rutas', services.rutas);

//Routes add-sensors
router.get('/add-sensors', services.add_sensor);

//Routes update-sensors
router.get('/update-sensors', services.update_sensor);

// API - CRUD
router.post('/api/sensors', controller.create);
router.get('/api/sensors', controller.find);
router.put('/api/sensors/:id', controller.update);
router.delete('/api/sensors/:id', controller.delete);

module.exports = router;