const express = require('express');
const app = express();
const path = require('path');
const bodyparser = require("body-parser");
const morgan = require('morgan');
const connectDB = require('./database/database');

//settings
app.set('port', 8080);
app.set('views', path.join(__dirname,'/view/'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
 
//MongoDB Connection
connectDB();

// log requests
app.use(morgan('tiny'));

// parse request to body-parser
app.use(bodyparser.json()); //Format JSON
app.use(bodyparser.urlencoded({ extended : true})) //Format URL

//static files
app.use(express.static(path.join(__dirname,'public')));

//routes
app.use(require('./routes'));

//listening the server
app.listen(app.get('port'), ()=>{
    console.log('El servidor esta en el puerto', app.get('port'));
});