var Sensordb = require('../model/model');

// create and save new sensor
exports.create = (req, res)=>{
    // validate request

    if (!req.body) {
        res.status(400).send({ message: "¡El contenido no puede estar vacío!" });
        return;
    }
    
    // new sensor
    const sensor = new Sensordb({
        Distancia:req.body.Distancia,
        Altitud:req.body.Altitud,
        Longitud:req.body.Longitud,
        Nivel:req.body.Nivel,
        Estado:req.body.Estado,
        Hora:req.body.Hora,
        Fecha:req.body.Fecha
    })
    
    // save sensor in the database
    sensor
        .save(sensor)
        .then(data => {
            res.send(data)
            //res.redirect('/add-sensors');
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Se produjo un error al crear una operación de creación"
            });
        });
}

// retrieve and return all sensor/ retrive and return a single sensor
exports.find = (req, res) => {
    if (req.query.id) {
        const id = req.query.id;

        Sensordb.findById(id)
            .then(data => {
                if (!data) {
                    res.status(404).send({ message: "No se encontro el sensor con el id: " + id })
                } else {
                    res.send(data)
                }
            })
            .catch(err => {
                res.status(500).send({ message: "Error en devolver el sensor con el id: " + id })
            })

    } else {
        Sensordb.find()
            .then(sensor => {
                res.send(sensor)
            })
            .catch(err => {
                res.status(500).send({ message: err.message || "Se produjo un error al recuperar la información del sensor" })
            })
    }


}

// Update a new idetified sensor by sensor id
exports.update = (req, res) => {
    if (!req.body) {
        return res
            .status(400)
            .send({ message: "!Los datos de los sensores no pueden estar vacio!" })
    }

    const id = req.params.id;
    Sensordb.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({ message: `No se puede actualizar el contenedor con ${id}. ¡Quizás no se encuentra el contenedor!` })
            } else {
                res.send(data)
            }
        })
        .catch(err => {
            res.status(500).send({ message: "Error al actualizar la Información del sensor" })
        })
}

// Delete a sensor with specified sensor id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Sensordb.findByIdAndDelete(id)
        .then(data => {
            if (!data) {
                res.status(404).send({ message: `No es posible eliminar contenedor con el id ${id}. ¡Tal vez el id esta mal!` })
            } else {
                res.send({
                    message: "Sensor eliminado correctamente!"
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "No se puede eliminar el sensor con el id = " + id
            });
        });
}